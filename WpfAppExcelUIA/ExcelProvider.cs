﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;
using System.Windows.Automation.Provider;

using Excel = Microsoft.Office.Interop.Excel;

namespace WpfAppExcelUIA
{
    // (1) Add UIAutomationTypes, UIAutomationProvider, UIAutomationClient, Microsoft.Office.Interop.Excel to References
    // (2) Define class UiaClientSideProviders
    public class UiaClientSideProviders
    {
        public static readonly string CLASS_EXCEL_EXCEL7 = "EXCEL7";
        static ClientSideProviderDescription[] ClientSideProviderDescriptionTable = 
        { 
            new ClientSideProviderDescription(
                ExcelProvider.Create,       // Method that creates the provider object.
                CLASS_EXCEL_EXCEL7          // Class of window that will be served by the provider.
                )
        };

        public static void RegisterClientSideProviders()
        {
            ClientSettings.RegisterClientSideProviders(ClientSideProviderDescriptionTable);
        }
    }

    class ExcelProvider : IRawElementProviderSimple, IGridProvider
    {
        // (3) Import DLLs
        [DllImport("user32.dll")]
        private static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        // (4) define Create method
        internal static IRawElementProviderSimple Create(IntPtr hwnd, int idChild, int idObject)
        {
            StringBuilder buffer = new StringBuilder(128);
            GetClassName(hwnd, buffer, buffer.Capacity);
            if (!buffer.ToString().Equals(UiaClientSideProviders.CLASS_EXCEL_EXCEL7)) return null;
            if (idChild != 0) return null;
            return new ExcelProvider(hwnd);
        }

        private IntPtr providerHwnd;
        internal Excel.Worksheet excelSheet;

        private ExcelProvider(IntPtr hwnd)
        {
            providerHwnd = hwnd;
            var excelApp = (Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
            if (excelApp == null) return;
            StringBuilder sb = new StringBuilder(1024);
            if (GetWindowText(hwnd, sb, 1024) <= 0) return;
            string title = sb.ToString();
            foreach (Excel.Workbook x in excelApp.Workbooks)
                if (x.Name == title || x.Name + " - Excel" == title)
                {
                    excelSheet = x.ActiveSheet;
                    break;
                }
        }

        // implement methods of IRawElementProviderSimple
        public object GetPropertyValue(int propertyId)
        {
            var property = AutomationProperty.LookupById(propertyId);

            if (property == AutomationElementIdentifiers.AutomationIdProperty)
                return this.ToString();
            if (property == AutomationElementIdentifiers.NameProperty)
                return "MyExcel7";
            return null;
        }
        public IRawElementProviderSimple HostRawElementProvider
        {
            get { return AutomationInteropProvider.HostProviderFromHandle(providerHwnd); }
        }
        public ProviderOptions ProviderOptions
        {
            get { return ProviderOptions.ClientSideProvider; }
        }

        public object GetPatternProvider(int patternId)
        {
            if (patternId == GridPatternIdentifiers.Pattern.Id) return this;
            return null;
        }

        public int ColumnCount
        {
            get { return excelSheet == null ? 0 : excelSheet.Columns.Count; }
        }

        public IRawElementProviderSimple GetItem(int row, int column)
        {
            return new ExcelCellProvider(this, row,column);
        }

        public int RowCount
        {
            get { return excelSheet==null?0:excelSheet.Rows.Count; }
        }
    }

    class ExcelCellProvider : IRawElementProviderSimple, IGridItemProvider
    {
        private object value, value2, text; // The only difference between value and value2 is for Currency and Date data types. 
        private int row, col;
        internal ExcelCellProvider(ExcelProvider parent, int row, int col)
        {
            this.row = row;
            this.col = col;
            if (parent.excelSheet == null) return;
            Excel.Range cell=null;
            try
            {
                cell = parent.excelSheet.Cells[row > 1 ? row : 1, col > 1 ? col : 1];
            }
            catch (Exception ex)
            {
                Console.WriteLine("** Ex: " + ex.Message);
            }
            if (cell == null) return;
            if (row == 0 && col == 0)
            {
                text = "[]";
                value = value2 = "";
            }
            else if (row == 0) 
            {
                text = String.Format("[{0}]",(char)(0x40+col));
                value = value2 = "";
            }
            else if (col == 0) 
            {
                text = String.Format("[{0}]", row);
                value = value2 = "";
            }
            else
            {
                try
                {
                    value = cell.Value;
                    value2 = cell.Value2;
                    text = cell.Text;
                }
                catch (Exception ex)
                {
                    text = ex.Message;
                }
            }
        }

        // IRawElementProviderSimple
        object IRawElementProviderSimple.GetPatternProvider(int patternId)
        {
            return null;
        }

        object IRawElementProviderSimple.GetPropertyValue(int propertyId)
        {
            var property = AutomationProperty.LookupById(propertyId);

            if (property == AutomationElementIdentifiers.AutomationIdProperty)
                return String.Format("{0}{1}", (char)(0x40 + col), row);
            if (property == AutomationElementIdentifiers.ClassNameProperty)
                return String.Format("{0}{1}", (char)(0x40 + col), row);
            if (property == AutomationElementIdentifiers.LocalizedControlTypeProperty)
                return "excel cell";
            if (property == AutomationElementIdentifiers.NameProperty)
                return text.ToString();
            if (property == AutomationElementIdentifiers.ItemStatusProperty)
                return String.Format("status:[{0},{1}]", row, col);
            if (property == AutomationElementIdentifiers.HelpTextProperty)
                return String.Format("{0}|{1}", value, value2);
            if (property == AutomationElementIdentifiers.IsContentElementProperty ||
                property == AutomationElementIdentifiers.IsControlElementProperty ||
                property == AutomationElementIdentifiers.NativeWindowHandleProperty ||
                property == AutomationElementIdentifiers.ControlTypeProperty ||
                property == AutomationElementIdentifiers.IsOffscreenProperty)
                return null;
            Console.WriteLine("???" + propertyId + "." + property);
            return null;

        }
        IRawElementProviderSimple IRawElementProviderSimple.HostRawElementProvider
        {
            get { return null; }
        }
        ProviderOptions IRawElementProviderSimple.ProviderOptions
        {
            get { return ProviderOptions.ClientSideProvider; ; }
        }

        // IGridItemProvider
        public int Column
        {
            get { return col; }
        }
        public int ColumnSpan
        {
            get { return 1; }
        }
        public IRawElementProviderSimple ContainingGrid
        {
            get { return this; }
        }
        public int Row
        {
            get { return row; }
        }
        public int RowSpan
        {
            get { return 1; }
        }
    }
}
