﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppExcelUIA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            UiaClientSideProviders.RegisterClientSideProviders();
        }
        AutomationElement excel7 = null;
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            button1.IsEnabled = false;
            AutomationElement aeDesktop = AutomationElement.RootElement;
            AutomationElement appRootElement = null;
            foreach (AutomationElement x in aeDesktop.FindAll(TreeScope.Children, System.Windows.Automation.Condition.TrueCondition))
            {
                if (Regex.IsMatch(x.Current.Name, textExcel.Text))
                {
                    appRootElement = x;
                    break;
                }
            }
            if (appRootElement == null)
            {
                button1.IsEnabled = true;
                msg.Text = "Excel app not found";
                return;
            }
            listBox1.Items.Add("Excel Application found");
            excel7 = findExcel7(appRootElement);
            if (excel7 == null)
            {
                msg.Text = "Excel7 class not found";
                return;
            }
            listBox1.Items.Add("Class Excel7 found");
            listBox1.Items.Add("Children of class Excel7:");
            button2.IsEnabled = true;
            foreach (AutomationElement x in excel7.FindAll(TreeScope.Children, System.Windows.Automation.Condition.TrueCondition))
            {
                listBox1.Items.Add(String.Format("\ttype:{0} class:{1} name:{2}",x.Current.LocalizedControlType,
                    x.Current.ClassName,x.Current.Name));
            }
            var pattern = (GridPattern)excel7.GetCurrentPattern(GridPattern.Pattern);
            if (pattern != null)
            {
                listBox1.Items.Add(String.Format("Excel7 rows:{0} columns:{1}", pattern.Current.RowCount, pattern.Current.ColumnCount));
            }
            return;
        }

        private AutomationElement findExcel7(AutomationElement root) 
        {
            foreach (AutomationElement x in root.FindAll(TreeScope.Children, System.Windows.Automation.Condition.TrueCondition))
            {
                if (x.Current.ClassName == UiaClientSideProviders.CLASS_EXCEL_EXCEL7) return x;
                var z = findExcel7(x);
                if (z != null) return z;
            }
            return null;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (excel7 != null && !Regex.IsMatch(textCell.Text,@"^[A-Z]\d+$")) return;
            var pattern = (GridPattern)excel7.GetCurrentPattern(GridPattern.Pattern);
            if (pattern != null)
            {
                var x = pattern.GetItem(int.Parse(textCell.Text.Substring(1)), textCell.Text[0]-'A'+1);
                listBox1.Items.Add(String.Format("{0}: {1} {2} {3}",textCell.Text, x.Current.Name, x.Current.ClassName, x.Current.HelpText));
            }

        }
    }
}
